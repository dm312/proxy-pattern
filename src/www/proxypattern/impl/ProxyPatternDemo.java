/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.proxypattern.impl;

import www.proxypattern.MyObject;
import www.proxypattern.impl.Shirt;
/**
 *
 * @author Danny
 */
public class ProxyPatternDemo {
 
public static void main(String[] args){
    
    String shirtType = "Plaid";
    MyObject originalShirt = new Shirt(shirtType);
    
    MyObject myobject = new ProxyShirt(originalShirt.getType());
     myobject.fold();
 }
}
