/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.proxypattern;

/**
 *
 * @author Danny
 */
public interface MyObject {
    void sit(String type);
    String getType();
    public void fold();
}
