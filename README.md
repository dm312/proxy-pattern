**Documentation for Proxy and Observer Pattern

The problem I solved with the proxy pattern was in the case of receiving 
a big Shirt object that is expensive to duplicate and instantiate in the case 
that a user wants to add further details. Because
it is expensive to duplicate
by wrapping the object in a Proxy object, the original object is kept while
providing functionality and logic to be acted on the object. This solves the 
problem of having to manipulate the original big Shirt shirt object by wrapping
it in a proxy so to use the proxy as a way of performing operations on the real
object in a way to save resources. So, because the original object is wrapped
in a proxy, the client may perform logic on it without having to use computer
resources to clone the original shirt object.

The problem I solved with the observer pattern was in the case of adding 
a new mailbox to be opened in a post office. When a new mailbox is added
the entire building should be notified that a new mailbox has been inserted 
in order to update performance checks on the mail such as inserting mail to the 
mailbox, updating account information such as address, first name, last name, etc.
By using the observer pattern, when a new mailbox is added, the clerk is notified
of all existing mailboxes. This is useful so checks are constantly performed
when a new mailbox is added. So the postoffice would be the subject, while the 
mailboxes are the dependents. When any mailbox is added an observer object is
attached to the mailbox and is automatically notified of a state change - when
a new address is added.