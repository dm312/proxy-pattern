/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.proxypattern.impl;

import www.proxypattern.MyObject;

/**
 *
 * @author Danny
 */
public class Shirt implements MyObject {

    public void sit(String type) {
        System.out.println(type + " shirt is on the table.");
    }

    public void fold() {
        System.out.println(type + " shirt is folded");
    }

    public Shirt(String type) {
        this.type = type;
        sit(type);

    }
    public String getType(){
        return this.type;
    }
    public static void main(String[] args) {
        Shirt shirt = new Shirt("Plaid");
        shirt.fold();
    }

    private String type;

}
