/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package www.proxypattern.impl;

import www.proxypattern.MyObject;

/**
 *
 * @author Danny
 */
public class ProxyShirt implements MyObject {

    @Override
    public void sit(String type) {
        if(shirt == null)
        {
            System.out.println("Shirt instance created");
            shirt = new Shirt(type);
            System.out.println("Reading shirt instance info");
            System.out.println(shirt.getType() + " shirt is on the table");
        }
        
    }
    
    public static void main(String[] args)
    {
        MyObject proxyShirt = new ProxyShirt("Plaid");
        proxyShirt.fold();
    }

    @Override
    public void fold() {
        if(shirt == null){
            System.out.println("Shirt instance created");
            shirt = new Shirt(this.type);
        }
        System.out.println("Reading Shirt instance info");
        System.out.println(shirt.getType() + " shirt is folded");
    }
    @Override
    public String getType(){
        return this.type; 
    }
    public ProxyShirt(String type)
    {
        this.type = type;
        sit(type);
    }
    
    private String type;
    private Shirt shirt;
}
